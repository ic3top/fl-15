const mainTable = document.getElementById('main-table');
const btnAll = document.getElementById('btn-all');
const btnLow = document.getElementById('btn-low')
const fragment = document.createDocumentFragment();

class Employee {
  constructor(infoObj) {
    Object.assign(this, infoObj);
    this.workers = [];
  }

  add(worker) {
    this.workers.push(worker);
  }

  hasChildren() {
    return this.workers.length > 0;
  }

  getChild(index) {
    return this.workers[index];
  }

  setLevel(num) {
    this.level = num;
  }

  getTableRow() {
    const tr = document.createElement('tr');
    let level = this.level;
    while (level > 1) {
      tr.append(document.createElement('td'));
      level--;
    }
    if (this.hasChildren()) {
      tr.insertAdjacentHTML('beforeend', `<td style="background-color: yellow">${this.name}</td>`);
    } else {
      tr.insertAdjacentHTML('beforeend', `<td>${this.name}</td>`);
    }

    return tr;
  }

  isLowPerfomance() {
    return this.perfomance === 'low';
  }
}

function traverse(indent, node) {
  const level = indent++;
  node.setLevel(level);
  fragment.append(node.getTableRow());
  for (let i = 0, len = node.workers.length; i < len; i++) {
    traverse(indent, node.getChild(i));
  }
}

function resetUI() {
  mainTable.innerHTML = '';
  fragment.innerHTML = '';
}


fetch('./mock.json').then(res => res.json()).then(data => {
  const allWorkers = data.map(person => new Employee(person));
  allWorkers.forEach(worker => {
    const rm = allWorkers.find(rm => rm.id === worker.rm_id);
    if (rm) {
      rm.add(worker);
    }
  });
  btnAll.addEventListener('click', () => {
    resetUI();
    traverse(1, allWorkers[0]);
    mainTable.append(fragment);
  });
  btnLow.addEventListener('click', () => {
    const lowPerfWorkers = allWorkers.filter(({ performance }) => performance === 'low');
    resetUI();
    lowPerfWorkers.forEach(person => {
      fragment.append(person.getTableRow());
    });
    mainTable.append(fragment);
  })
});