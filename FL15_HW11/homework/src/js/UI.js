export const UI = {
  message: document.getElementById('message'),
  instruction: document.getElementById('instruction'),
  boxes: document.querySelectorAll('.box'),
  reset: document.getElementById('reset-btn'),
  player1Wins: document.getElementById('player1-wins'),
  player2Wins: document.getElementById('player2-wins'),
  draw: document.getElementById('draws'),
  container: document.getElementById('boxes-container')
}