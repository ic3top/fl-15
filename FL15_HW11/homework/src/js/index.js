import '../scss/style.scss';
import { UI } from './UI';
import { toggleTurn } from './utils/toggleTurn';
import { checkForWinner } from './utils/checkForWinner';
import { resetBoard } from './utils/resetBoard';

UI.container.addEventListener('click', boardClick);

function boardClick(event) {
  if (event.target.id < 10 && !event.target.textContent) {
    event.target.textContent = toggleTurn();
    const result = checkForWinner();
    if (result) {
      UI[result].textContent++;
      UI.container.removeEventListener('click', boardClick);
    }
  }
}  

UI.reset.addEventListener('click', () => {
  resetBoard();
  UI.container.addEventListener('click', boardClick);
});