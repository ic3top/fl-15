import { UI } from '../UI';
import { highlight } from './highlight';
import { isWon } from './isWon';

const declareWinner = (msg) => {
  UI.message.textContent = msg;
  UI.instruction.textContent = 'yay!';
};

export const checkForWinner = () => {
  let xArray = [];
  let oArray = [];

  UI.boxes.forEach((box) => {
    if (box.textContent === 'X') {
      xArray.push(parseInt(box.id));
    }
    if (box.textContent === 'O') {
      oArray.push(parseInt(box.id));
    }
  });
  if (!(xArray.length > 2 || oArray.length > 2)) {
    return; 
  }

  const xCombo = isWon(xArray);
  const oCombo = isWon(oArray);

  if (xCombo) {
    declareWinner('Crosses win!');
    highlight(xCombo);

    return 'player1Wins';
  } 

  if (oCombo) {
    declareWinner('Noughts win!');
    highlight(oCombo);

    return 'player2Wins';
  }

  if (xArray.length + oArray.length === 9) {
    declareWinner('Draw');
    
    return 'draw';
  }
}