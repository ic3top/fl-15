import { UI } from '../UI';

export const highlight = (winCode) => {
  for (let i = 0; i < 3; i++) {
    const el = UI.boxes[winCode[i]];
    el.style.background = 'lightcyan';
  }
}

export const resetHighlight = () => {
  UI.boxes.forEach((box) => {
      box.style.background = 'white';
    }
  );
}