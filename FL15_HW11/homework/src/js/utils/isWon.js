const winningArrays = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

export const isWon = (playerArray) => {
  let winCode;
  let final = false;
  winningArrays.forEach((combo) => {
    for(let i = 0; i < 3; i++) {
      if ( playerArray.indexOf(combo[i]) === -1 ) {
        return false; 
      }
    }
    winCode = combo;
    final = true
  });

  if ( final ) {
    return winCode; 
  }

  return false;
}