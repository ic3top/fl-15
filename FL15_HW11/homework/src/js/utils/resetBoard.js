import { UI } from '../UI';
import { resetHighlight } from './highlight';

export const resetBoard = () => {
  UI.boxes.forEach((el) => {
    el.textContent = '';
  });
  
  resetHighlight();
  UI.message.textContent = `Crosses start`;
  UI.instruction.textContent = `Click in a box to play`;
}