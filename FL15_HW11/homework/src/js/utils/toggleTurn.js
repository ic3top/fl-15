import { UI } from '../UI';

export const toggleTurn = (() => {
  let icon = 'O';
  return () => {
    const temp = icon;
    if ( icon === 'X' ) {
      icon = 'O';
      UI.instruction.textContent = "Nought's turn";
    } else {
      icon = 'X';
      UI.instruction.textContent = "Cross's turn";
    }
    return temp; 
  }
})();