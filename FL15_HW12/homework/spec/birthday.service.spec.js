const BirthdayService = require('../src/birthday.service');
const { congratulateWithBirthday } = require('../src/birthday.service');

describe('BirthdayService module', () => {
  describe('howLongToMyBirthday method', () => {
    it('howLongToMyBirthday method should be rejected with error if argument is not a date object', async () => {
      const err = BirthdayService.howLongToMyBirthday('not a date object');
  
      return err.catch(error => {
        expect(error).toBeInstanceOf(Error);
      });
    });
  
    it('congratulateWithBirthday method shoud be called', async () => {
      spyOn(BirthdayService, 'congratulateWithBirthday');
      await BirthdayService.howLongToMyBirthday(new Date());
  
      expect(BirthdayService.congratulateWithBirthday).toHaveBeenCalledTimes(1);
    });
  
    it('notifyWaitingTime method shoud be called with +10 days', async () => {
      spyOn(BirthdayService, 'notifyWaitingTime');
      const tenDaysForward = new Date(Date.now() + BirthdayService['ONE_DAY'] * 10)
      await BirthdayService.howLongToMyBirthday(tenDaysForward);
  
      expect(BirthdayService.notifyWaitingTime).toHaveBeenCalledOnceWith(10);
    });
  
    it('notifyWaitingTime method shoud be called with -45 days', async () => {
      spyOn(BirthdayService, 'notifyWaitingTime');
      const daysBack = new Date(Date.now() - BirthdayService['ONE_DAY'] * 45)
      await BirthdayService.howLongToMyBirthday(daysBack);
  
      expect(BirthdayService.notifyWaitingTime).toHaveBeenCalledOnceWith(-45);
    });
  });

  describe('Logger property, console output', () => {
    it('should add to logger congratulations message', async () => {
      const msg = '🎂 Hooray! 🎆 It is today!';
      spyOn(BirthdayService.logger, 'add');
      await BirthdayService.howLongToMyBirthday(new Date());
  
      expect(BirthdayService.logger.add).toHaveBeenCalledWith(msg)
    });
  
    it('should add to logger "celebrated" message with 45 days', async () => {
      const msg = 'Oh, you have celebrated it 45 days ago, don\'t you remember?';
      const daysBack = new Date(Date.now() - BirthdayService['ONE_DAY'] * 45)
      spyOn(BirthdayService.logger, 'add');
      await BirthdayService.howLongToMyBirthday(daysBack);
  
      expect(BirthdayService.logger.add).toHaveBeenCalledWith(msg)
    });
  
    it('should add to logger "wait" message with 10 days', async () => {
      const msg = 'Soon...Please, wait just 10 days';
      spyOn(BirthdayService.logger, 'add');
      const tenDaysForward = new Date(Date.now() + BirthdayService['ONE_DAY'] * 10)
      await BirthdayService.howLongToMyBirthday(tenDaysForward);
  
      expect(BirthdayService.logger.add).toHaveBeenCalledWith(msg)
    });
    
    it('should add to logger "celebrated" message with 1 days', async () => {
      const msg = 'Oh, you have celebrated it 1 day ago, don\'t you remember?';
      const dayBack = new Date(Date.now() - BirthdayService['ONE_DAY'])
      spyOn(BirthdayService.logger, 'add');
      await BirthdayService.howLongToMyBirthday(dayBack);
  
      expect(BirthdayService.logger.add).toHaveBeenCalledWith(msg)
    });
  
    it('should add to logger "wait" message with 11 days', async () => {
      const msg = 'Soon...Please, wait just 1 day';
      spyOn(BirthdayService.logger, 'add');
      const oneDayForward = new Date(Date.now() + BirthdayService['ONE_DAY'])
      await BirthdayService.howLongToMyBirthday(oneDayForward);
  
      expect(BirthdayService.logger.add).toHaveBeenCalledWith(msg)
    });
    
    it('should output each result', async () => {
      spyOn(BirthdayService.logger, 'show');
      await BirthdayService.howLongToMyBirthday(new Date(2022, 1, 1));
      await BirthdayService.howLongToMyBirthday(new Date(2023, 1, 1));
      await BirthdayService.howLongToMyBirthday(new Date(2024, 1, 1));
      BirthdayService.howLongToMyBirthday(new Date(2025, 1, 1));
  
      expect(BirthdayService.logger.show).toHaveBeenCalledTimes(4);
    });
  });
});