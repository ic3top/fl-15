const getBiggestNumber = require('../src/get-bigest-number');

describe('getBiggestNumber module', () => {
  it('should return the biggest number', () => {
    const result = getBiggestNumber(1, 432, 34, 3.2, 23, 65, -23, 0);
    expect(result).toBe(432);
    
    const negResult = getBiggestNumber(-1, -432, -34, -3.2, -23, -65, -23, -0);
    expect(negResult).toBe(0);
  });
  it('should throw an ERROR when at least one argument not typeof number', () => {
    const err = Error('Wrong argument type');
    expect(() => getBiggestNumber(1, 2, '', 3)).toThrow(err);
    expect(() => getBiggestNumber(1, 2, {}, 3)).toThrow(err);
    expect(() => getBiggestNumber([], 2, 3, 5)).toThrow(err);
    expect(() => getBiggestNumber(Function, 32, 43, 0)).toThrow(err);
  });
  it('should throw an ERROR if amount of arguments < 2', () => {
    const err = Error('Not enough arguments');
    expect(() => getBiggestNumber(1)).toThrow(err);
    expect(() => getBiggestNumber()).toThrow(err);
  });
  it('should throw an ERROR if amount of arguments > 10', () => {
    const err = Error('Too many arguments');
    const args = Array(22)
    expect(() => getBiggestNumber(...args)).toThrow(err);
    expect(() => getBiggestNumber('', 2, 3, 4, 5, 6, [9], 8, 9, 10, 11)).toThrow(err);
  });
});