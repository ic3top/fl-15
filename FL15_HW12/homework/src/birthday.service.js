module.exports = {
  howLongToMyBirthday: function (date) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (!(date instanceof Date)) {
          reject(new Error('Wrong argument!'));
        }

        const daysDiff = Math.round((date - new Date()) / this['ONE_DAY']);
        resolve(daysDiff);
      }, 100)
    })
    .then(days => {
      if (days === 0) {
        this.logger.add(this.congratulateWithBirthday());
      } else { 
        this.logger.add(this.notifyWaitingTime(days)) 
      }
      return days;
    })
    .then(this.logger.show());
  },

  congratulateWithBirthday: function () {
    return '🎂 Hooray! 🎆 It is today!';
  },

  notifyWaitingTime: function (days) {
    if (days < 0) {
      days = Math.abs(days);
      return `Oh, you have celebrated it ${days} ${days > 1 ? 'days' : 'day'} ago, don't you remember?`;
    }
    return `Soon...Please, wait just ${days} ${days > 1 ? 'days' : 'day' }`;
  },

  ONE_DAY: 1000 * 60 * 60 * 24,

  logger: (function () {
    let log = '';

    return {
      add: function (msg) {
        log += msg + '\n';
      },
      show: function () {
        console.log(log);
        log = '';
      }
    }
  })()
};