module.exports = function (...numbers) {
  if (numbers.length > 10) {
    throw Error('Too many arguments');
  }

  if (numbers.length < 2) {
    throw Error('Not enough arguments');
  }
  let largest = numbers[0];

  for (const num of numbers) {
    if (typeof num !== 'number') { 
      throw Error('Wrong argument type');
    }

    if (largest < num) {
      largest = num;
    }
  }

  return largest;
}