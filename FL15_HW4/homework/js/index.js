const faces = {
  1: 'Ace',
  11: 'Jack',
  12: 'Queen',
  13: 'King'
}
const suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];

class Deck {
  constructor() {
    this.cards = Deck.shuffle(Deck.generateDeck());
    this._count = this.cards.length;
  }

  get count() {
    return this._count;
  }

  draw(n) {
    this._count -= n;
    return this.cards.splice(-n, n);
  }

  static shuffle(deck) {
    for (let i = deck.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [deck[i], deck[j]] = [deck[j], deck[i]];
    }

    return deck;
  }

  static generateDeck() {
    const newDeck = [];
    for (const face of suits) {
      for (const rank of [...Array(13).keys()]) {
        newDeck.push(new Card(face, rank+1))
      }
    }

    return newDeck;
  }
}

class Card {
  constructor(suit, rank) {
    this.suit = suit;
    this.rank = rank;
    this.isFaceCard = rank === 1 || rank > 10;
  }

  toString() {
    return `${faces[this.rank] || this.rank} of ${this.suit}`;
  }

  static compare(cardOne, cardTwo) {
    if (cardOne > cardTwo) {
      return 1;
    }
    if (cardOne < cardTwo) {
      return -1;
    }

    return 0;
  }
}

class Player {
  constructor(name) {
    this.name = name;
    this.deck = new Deck();
    this._wins = 0;
  }

  get wins() {
    return this._wins;
  }

  static play(playerOne, playerTwo) {
    let pointsOne = 0;
    let pointsTwo = 0;
    while (playerOne.deck.count > 0) {
      const playerOneCard = playerOne.deck.draw(1);
      const playerTwoCard = playerTwo.deck.draw(1);
      const compare = Card.compare(playerOneCard, playerTwoCard);
      if (compare > 0) {
        pointsOne += 1;
        continue;
      }
      if (compare < 0) {
        pointsTwo += 1;
      }
    }

    playerOne.deck = new Deck();
    playerTwo.deck = new Deck();

    if (pointsOne > pointsTwo) {
      playerOne._wins += 1;
      return `${playerOne.name} wins ${pointsOne} to ${pointsTwo}`;
    }
    playerTwo._wins += 1;
    return `${playerTwo.name} wins ${pointsTwo} to ${pointsOne}`
  }
}

function calculateAge(dob) { 
  if (!dob) {
    return
  }
  const diff_ms = Date.now() - dob.getTime();
  const age_dt = new Date(diff_ms); 

  return Math.abs(age_dt.getUTCFullYear() - 1970);
}

class Employee {
  constructor(dataObj) {
    if(!Employee.EMPLOYEE) {
      Employee.EMPLOYEE = []
    }
    Employee.EMPLOYEE.push(this);

    Object.assign(this, dataObj);
    this.age = calculateAge(this.birthday);
    this.fullName = `${this.lastName} ${this.firstName}`;
  }

  quit() {
    Employee.EMPLOYEE = Employee.EMPLOYEE.filter((employee) => employee !== this);
  }

  retire() {
    console.log('It was such a pleasure to work with you!');
    this.quit();
  }

  getFired() {
    console.log('Not a big deal!');
    this.quit();
  }

  changeDepartment(newDep) {
    this.department = newDep;
  }

  changePosition(newPos) {
    this.position = newPos;
  }

  changeSalary(newSalary) {
    this.salary = newSalary;
  }

  getPromoted({ salary, position, department }) {
    salary ? this.changeSalary(salary): null;
    position ? this.changePosition(position): null;
    department ? this.changeDepartment(department): null;
    console.log('Yoohooo!');
  }

  getDemoted({ salary, position, department }) {
    salary ? this.changeSalary(salary): null;
    position ? this.changePosition(position): null;
    department ? this.changeDepartment(department): null;
    console.log('Damn!');
  }
}

class Manager extends Employee {
  constructor(dataObj) {
    super(dataObj);
    this.position = 'manager';
    this._managedEmployees = Employee.EMPLOYEE.filter((employee) => {
      return employee.department === this.department && employee.constructor === Employee
    });
  }
}

class BlueCollarWorker extends Employee {}

class HRManager extends Manager {
  constructor(dataObj) {
    super(dataObj);
    this.department = 'hr';
  }
}

class SalesManager extends Manager {
  constructor(dataObj) {
    super(dataObj);
    this.department = 'sales';
  }
}

function ManagerPro(manager) {
  if (manager instanceof Manager) {
    manager.canPromote = true;
  }
}