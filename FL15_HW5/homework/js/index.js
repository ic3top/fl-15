const title = document.getElementById('title');
const usersList = document.getElementById('users-container');
const postsList = document.getElementById('posts-container');
const loader = document.getElementById('loader');
const editForm = document.forms[0];
const editButton = document.getElementById('finalEditButton');
const data = [];

function showLoader(isLoading) {
  window.scrollTo(0, 0)
  if (isLoading) {
    loader.style.display = 'flex';
    return;
  }
  loader.style.display = 'none';
}

function renderUser(user) {
  const li = document.createElement('li');
  const ul = document.createElement('ul');
  ul.innerHTML = `
    <li>Username: ${user.username}</li>
    <li>Email: ${user.email}</li>
    <li>Phone: ${user.phone}</li>
    <li>Website: <a href="https://${user.website}">${user.website}</a></li>
    <button onclick="deleteUser(${user.id})">Delete user</button>
    <button onclick="editUser(${user.id})">Edit</button>
  `
  li.innerHTML = `<a onclick="showActivity(${user.id})">${user.name}</a>`;
  li.append(ul)
  li.id = user.id;
  return li;
}

function renderPost(post) {
  const li = document.createElement('li');
  const content = document.createElement('div');
  content.innerHTML = `
  <div>
    <h3>Title: ${post.title}</h3>
    <p>Body: ${post.body}</p>
    <h4>Comments:</h4>
  </div>
  `
  post.comments.forEach((comment) => {
    content.innerHTML += `
      <h5>${comment.name}</h5>
      <p>${comment.body}</p>
    `
  });
  li.append(content);
  return li;
}

function showAllusers() {
  location.hash = '';
  showLoader(true);
  fetch('https://jsonplaceholder.typicode.com/users/')
  .then((response) => response.json())
  .then((json) => {
    json.forEach(user => {
      usersList.append(renderUser(user));
    })
    showLoader(false);
  });
}

function deleteUser(id) {
  showLoader(true);
  fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
    method: 'DELETE'
  })
  .then(() => document.getElementById(`${id}`).remove())
  .then(() => showLoader());
}

function editUser(id) {
  location.hash = `edit=${id}`;
  showLoader(true);
  fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
  .then((response) => response.json())
  .then((user) => {
    editForm.userName.value = user.username;
    editForm.fullName.value = user.name;
    editForm.email.value = user.email;
    editForm.phone.value = user.phone;
    editForm.website.value = user.website;
    showLoader(false);
  });
}

function finalEdit() {
  showLoader(true);
  fetch('https://jsonplaceholder.typicode.com/users/' + location.hash.match(/\d+/)[0], {
  method: 'PUT',
  body: JSON.stringify({
    username: editForm.userName.value,
    name: editForm.fullName.value,
    email: editForm.email.value,
    phone: editForm.phone.value,
    website: editForm.website.value
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8'
  }
})
  .then((response) => response.json())
  .then((editedUser) => {
    console.log(editedUser);
    const editedLi = renderUser(editedUser);
    usersList.replaceChild(editedLi, document.getElementById(editedUser.id));
    location.hash = '';
    showLoader(false);
  });
}

function showActivity(id) {
  location.hash = 'posts=' + id;
  showLoader(true);
  fetch('https://jsonplaceholder.typicode.com/posts?userId=' + id)
    .then(res => res.json())
    .then(posts => posts.forEach(post => {
      getComments(post.id)
        .then(res => res.json())
        .then(comments => {
          post.comments = comments;
          postsList.append(renderPost(post));
        });
    }))
    .then(() => showLoader(false));
}

function getComments(postId) {
  return fetch('https://jsonplaceholder.typicode.com/comments?postId=' + postId);
}

function locationHashChanged() {
  if (/edit/.test(location.hash)) {
    title.textContent = `Edit user data`;
    usersList.style.display = 'none';
    postsList.style.display = 'none';
    editForm.style.display = 'flex';
    return;
  }

  if(/posts/.test(location.hash)) {
    title.textContent = `All posts by user`;
    postsList.style.display = 'block';
    usersList.style.display = 'none';
    editForm.style.display = 'none';
    return;
  }

  title.textContent = 'Users';
  usersList.style.display = 'block';
  postsList.style.display = 'none';
  editForm.style.display = 'none';
}

editButton.addEventListener('click', (e) => {
  e.preventDefault();
  finalEdit();
});

window.onhashchange = locationHashChanged;

showAllusers();