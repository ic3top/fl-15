// task 1
function maxElement(arr) {
  return Math.max(...arr);
}

// task 2
function copyArray(arr) {
  return [...arr];
}

// task 3
function addUniqueId(obj) {
  return Object.assign({}, obj, { id: Symbol('id') })
}

// task 4
function regroupObj({name, details: { id, age, university }}) {
  return { university, user: { age, firstName: name, id } };
}

// task 5
function findUniqueElement(arr) {
  return [...new Set(arr)];
}

// task 6
const hideNumber = (phone) => phone.slice(-4).padStart(phone.length, '*');

// task 7
const isRequired = () => {
 throw new Error('param is required'); 
};

const add = (a = isRequired(), b = isRequired()) => a + b;

// task 8
function getNames() {
  const result = [];

  return fetch('https://jsonplaceholder.typicode.com/users/')
    .then(res => res.json())
    .then(data => data.sort((prevUser, nextUser) => {
      return prevUser.name.localeCompare(nextUser.name)
    }))
    .then(sortedUsers => {
      sortedUsers.forEach(({ name }) => result.push(name))
      console.log(result);
      return result;
    })
}

// task 9
async function getNamesAsync() {
  const result = [];

  const res = await fetch('https://jsonplaceholder.typicode.com/users/');
  const data = await res.json();
  const sortedUsers = data.sort((prevUser, nextUser) => {
    return prevUser.name.localeCompare(nextUser.name);
  });
  sortedUsers.forEach(({ name }) => result.push(name));
  console.log(result);
  return result;
}