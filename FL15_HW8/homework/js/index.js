const $list = $('.list');
const $input = $('#add-input');
const $add = $('#add-submit');

let todos = [];

function saveToStorage(arr) {
  localStorage.setItem('todos', JSON.stringify(arr));
}

const localStorageSaver = {
  set: function(arr, prop, value) {
    if (prop === 'length') {
      return true;
    }

    arr.push(value);
    saveToStorage(arr);
    return true;
  }
};

// set proxy
const todosProxified = new Proxy(todos, localStorageSaver);

JSON.parse(localStorage.getItem('todos') || '[]').forEach(({ text, done }) => {
  addNewItem(text, done);
});

function addNewItem(text, done=false) {
  const itemHTML = `
  <li id="${todos.length}" class="item">
    <span class="item-text ${done ? 'done' : ''}">${text}</span>
    <button class="item-remove">Remove</button>
  </li>`;
  todosProxified.push({ id: todos.length, text, done })
  $list.append(itemHTML);
}

$add.click(function(e) {
    e.preventDefault();
    const toAdd = $input.val().trim();
    if (!toAdd) {
      console.log('ERROR: Empty input');
      return;
    }
    addNewItem(toAdd);
    $input.val('');
  }
);

$input.keyup(function(e) {
  if(e.keyCode === 13){
    $add.click();
  }         
});

$list.on('click', 'li', function (e) {
  if (e.target.nodeName === 'BUTTON') {
    this.remove();  
    todos = todos.filter(({ id }) => id != this.id);
    saveToStorage(todos);
    return; 
  }
  const textSpan = $(this).find('.item-text');

  textSpan.toggleClass('done');
  if (textSpan.hasClass('done')) {
    todos.find((item) => item.id == this.id).done = true;
    saveToStorage(todos);
    return;
  }
  todos.find((item) => item.id == this.id).done = false;
  saveToStorage(todos);
});


