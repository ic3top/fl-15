import '../styles/style.sass';
import UI from './services/UI';
import { setResultMessage, setWins, setLosses, setDraws } from './services/setResult';
import getAIResult from './helpers/getAIResult';

let playerWin = 0;
let aiWin = 0;
let draws = 0;
let playerRoll; // rock = 1, paper = 2, scissors = 3

UI.rock.addEventListener('click', () => {
  playerRoll = 1;
  const computerRoll = getAIResult();
    // draw
  if (computerRoll === playerRoll) {
    setResultMessage('draw', 'rock');
    setDraws(++draws);
    // rock vs AI paper
  } else if (playerRoll === 1 && computerRoll === 2) {
    setResultMessage('lost', 'paper');
    setLosses(++aiWin);
    // rock vs AI scissors
  } else {
    setResultMessage('won', 'scissors');
    setWins(++playerWin);
  }  
});

// paper click
UI.paper.addEventListener('click', () => {
  playerRoll = 2;
  const computerRoll = getAIResult();
  
  // draw
  if (computerRoll === playerRoll) {
    setResultMessage('draw', 'paper')
    setDraws(++draws);
  // paper vs AI rock
  } else if (playerRoll === 2 && computerRoll === 1) {
    setResultMessage('won', 'rock');  
    setWins(++playerWin);
  // paper vs AI scissors
  } else {
    setResultMessage('lost', 'scissors');
    setLosses(++aiWin);
  }
});

// scissors click
UI.scissors.addEventListener('click', () => {
  playerRoll = 3;
  const computerRoll = getAIResult();

    // draw
  if (computerRoll === playerRoll) {
    setResultMessage('draw', 'scissors');
    setDraws(++draws);
    // scissors vs AI paper
  } else if (playerRoll === 3 && computerRoll === 2) {
    setResultMessage('won', 'paper');
    setWins(++playerWin);
    // scissors vs AI rock
  } else {
    setResultMessage('lost', 'rock');
    setLosses(++aiWin);
  }
});