const UI = {
  gameMessage: document.getElementById('game-message'),
  scoreBoard: document.getElementById('scoreboard'),
  rock: document.getElementById('rock'),
  paper: document.getElementById('paper'),
  scissors: document.getElementById('scissors'),
  buttonRow: document.getElementById('button-row'),
  draws: document.getElementById('draws'),
  wins: document.getElementById('wins'),
  losses: document.getElementById('losses'),
  result: document.getElementById('result')
}

export default UI;