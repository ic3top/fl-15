import UI from './UI';

export function setResultMessage(result, computerCoise) {
  UI.gameMessage.innerHTML = `
    <p class="game-${result}">${result}!</p>
    <p class="computer-message">The Computer chose ${computerCoise}!</p>
  `;
}

export const setDraws = (amount) => {
  UI.draws.textContent = amount;
};

export const setLosses = (amount) => {
  if (amount > 2) {
    UI.gameMessage.style.display = 'none';
    UI.buttonRow.style.visibility = 'hidden';
    UI.result.style.color = '#900';
    UI.result.textContent = 'Computer won.';
  }
  UI.losses.textContent = amount;
};

export const setWins = (amount) => {
  if (amount > 2) {
    UI.gameMessage.style.display = 'none';
    UI.buttonRow.style.visibility = 'hidden';
    UI.result.style.color = 'green';
    UI.result.textContent = 'You won!';
  }
  UI.wins.textContent = amount;
};