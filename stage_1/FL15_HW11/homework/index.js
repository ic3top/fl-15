function isEquals(a, b) {
  return a === b;
}

function isBigger(bigger, smaller) {
  return bigger > smaller;
}

function storeNames(...rest) {
  return rest;
}

function getDifference(a, b) {
  return a > b ? a-b : b-a;
}

function negativeCount(arr) {
  return arr.filter((el) => el < 0).length;
}

function letterCount(str, toFind) {
  return str.split('').filter((letter) => letter.toLowerCase() === toFind.toLowerCase()).length;
}

function countPoints(arr) {
  let points = 0;
  arr.forEach((str) => {
    const [x, y] = str.split(':').map((str) => parseInt(str));
    if(x > y) {
      points += 3;
    } else if(x === y) {
      points += 1;
    }
  });

  return points;
}