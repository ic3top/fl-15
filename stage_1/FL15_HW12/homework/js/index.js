function visitLink(path) {
	if(localStorage[path]) {
    localStorage[path] = parseInt(localStorage[path]) + 1;
    return;
  }

  localStorage[path] = 1;
}

function viewResults() {
  const ul = document.createElement('ul');
  for (const page of Object.keys(localStorage)) {
    const li = document.createElement('li');
    li.textContent = `You visited ${page} ${localStorage[page]} time(s)`;
    ul.append(li);
  }
  if(ul.innerHTML) {
    document.querySelector('#content').insertAdjacentElement('beforeend', ul);
  }
  localStorage.clear();
}
