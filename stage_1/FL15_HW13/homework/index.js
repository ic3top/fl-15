function getAge(birthDate) {
  const today = new Date();
  const age = today.getFullYear() - birthDate.getFullYear();

  return age;
}

function getWeekDay(time) {
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  if (time instanceof Date) {
    return days[time.getDay()];
  }
  const date = new Date(time*1000);
  return days[date.getDay()];
}

function getAmountDaysToNewYear() {
  const now = new Date();
  const christmas = new Date(now.getFullYear(), 11, 25);
  const day = 1000*60*60*24;
  return Math.ceil((christmas.getTime() - now.getTime())/day);
}

function getProgrammersDay(year) {
  const leap = new Date(year, 1, 29).getDate() === 29;
  if (leap) {
    return `12 Sep, ${year} (${getWeekDay(new Date(year, 8, 12))})`
  }

  return `13 Sep, ${year} (${getWeekDay(new Date(year, 8, 13))})`
}

function howFarIs(day) {
  const dayms = 1000*60*60*24;
  const given = day[0].toUpperCase() + day.substring(1);
  const today = getWeekDay(new Date());
  
  if (given === today) {
    return `Hey, today is ${ given } =)`
  }
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  if (days.indexOf(given) > days.indexOf(today)) {
    const number = ( new Date(2020, 0, days.indexOf(given)) - new Date(2020, 0, days.indexOf(today)) ) / dayms;
    return `It's ${ number } day(s) left till ${ given }`;
  }

  const number = ( new Date(2020, 0, days.indexOf(given)+7) - new Date(2020, 0, days.indexOf(today)) ) / dayms;
  return `It's ${ number } day(s) left till ${ given }`;
}

function isValidIdentifier(str) {
  const reg = /^\D[a-z0-9_$]+$/i;
  return reg.test(str);
}

function capitalize(str) {
  return str.replace(/(^\w|\s\w)/g, (m) => m.toUpperCase());
}

function isValidAudioFile(str) {
  console.log(/^([a-zA-Z]+\.(mp3|flack|alac|aac)$)/.test(str));
}

function getHexadecimalColors(text) {
  const result = [];
  text.replace(/;/g, ' ').split(' ').forEach((str) => {
    if (/^#(?:[0-9a-fA-F]{3}){1,2}$/.test(str)) {
      result.push(str);
    }
  });
  return result;
}

function isValidPassword(password) {
  return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,}$/.test(password);
}

function addThousandsSeparators(number) {
  return String(number).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}


function getAllUrlsFromText(text) {
  if (text) {
    return text.match(/https?:\/\/[-a-zA-Z0-9@:%._+~#=]+\.[a-zA-Z0-9()]+\b([a-zA-Z0-9()@:%_+.~#?&/=]*)/g) || [];
  }
  
  return new Error('Invalid input data');
}