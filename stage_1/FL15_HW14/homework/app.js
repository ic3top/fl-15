const appRoot = document.getElementById('app-root');
const title = document.createElement('h1');
const form = document.createElement('form');
const table = document.createElement('table');
let state = 'name-ascending';

function insertElement(element) {
  appRoot.insertAdjacentElement('beforeend', element);
}

function insertHTML(htmlString) {
  appRoot.insertAdjacentHTML('beforeend', htmlString);
}

function getData(type) {
  if (type === 'region') {
    // eslint-disable-next-line no-undef
    return externalService.getCountryListByRegion(select.value);
  }
  // eslint-disable-next-line no-undef
  return externalService.getCountryListByLanguage(select.value);
}

function sortAlphabeticallyByName(arr) {
  const result = arr.slice(0);
  return result.sort(function (a, b) {
    const x = a.name.toLowerCase();
    const y = b.name.toLowerCase();
    if (x < y) {
      return -1;
    }
    if (x > y) {
      return 1;
    }
    return 0;
  });
}

function sortByArea(arr) {
  const result = arr.slice(0);
  return result.sort((a, b) => a.area - b.area);
}

function generateDefaultStr() {
  const plug = document.getElementById('plug');
  if (!plug) {
    insertHTML('<p id="plug">No items, please choose search query</p>');
    if (table) {
      removeAllTd();
      table.remove();
    }
  }
}

function removeDefaultStr() {
  const plug = document.getElementById('plug');
  if (plug) {
    plug.remove();
  }
}

function removeOptions(element) {
  while (element.options.length > 1) {
    element.remove(1);
  }
}

function generateOptions(type) {
  let list;
  if (type === 'region') {
    // eslint-disable-next-line no-undef
    list = externalService.getRegionsList();
  } else if (type === 'language') {
    // eslint-disable-next-line no-undef
    list = externalService.getLanguagesList();
  } else {
    throw Error(`Invalid type - ${type}`)
  }
  generateDefaultStr();
  select.removeAttribute('disabled');
  removeOptions(select);
  for (let item of list) {
    const option = document.createElement('option');
    option.value = item;
    option.text = item;
    select.add(option);
  }
}

function removeAllTd() {
  const allTd = document.getElementsByClassName('js-tr-details');
  [...allTd].map((el) => el.remove());
}

function generateTr(details) {
  for (let detail of details) {
    const tr = getTr(detail);
    table.append(tr);
  }
}

function generateTable(details) {
  const sortedDetailsByName = sortAlphabeticallyByName(details);
  removeAllTd();
  generateTr(sortedDetailsByName);
  insertElement(table);
  displayArrowUp(document.getElementById('arrow-name'));
  displayArrowDouble(document.getElementById('arrow-area'));
  initializeSorters();
}

function getTr({name, flagURL, capital, area, languages, region}) {
  const tr = document.createElement('tr');
  tr.classList.add('js-tr-details');
  const languagesStr = Object.values(languages).reduce((lang, string) => string + ', ' + lang, '').slice(0, -2);
  tr.innerHTML =
    `
      <td class="js-td-name">${name}</td>
      <td>${capital}</td>
      <td>${region}</td>
      <td>${languagesStr}</td>
      <td>${area}</td>
      <td><img src="${flagURL}" alt="flag of the ${name}"></td>
    `;

  return tr;
}

function displayArrowDown(arrowHTML) {
  if (arrowHTML) {
    arrowHTML.innerHTML = '&#8595';
  }
}

function displayArrowUp(arrowHTML) {
  if (arrowHTML) {
    arrowHTML.innerHTML = '&#8593';
  }
}

function displayArrowDouble(arrowHTML) {
  if (arrowHTML) {
    arrowHTML.innerHTML = '&#8597';
  }
}

function initializeSorters() {
  const arrowName = document.getElementById('arrow-name');
  const arrowArea = document.getElementById('arrow-area');
  // To prevent multiply events listeners
  arrowArea.removeEventListener('click', sortOnArrowName);
  arrowName.removeEventListener('click', sortOnArrowArea);
  // Sort by country name
  arrowName.addEventListener('click', sortOnArrowName);
  // Sort by area
  arrowArea.addEventListener('click', sortOnArrowArea);
}

function sortOnArrowName() {
  const arrowArea = document.getElementById('arrow-area');
  const [{value}] = [...radioList].filter(radio => radio.checked);
  removeAllTd();
  let sortedByName;
  if (state === 'name-ascending') {
    sortedByName = sortAlphabeticallyByName(getData(value)).reverse();
    state = 'name-descending';
    displayArrowDown(this);
    displayArrowDouble(arrowArea);
  } else {
    sortedByName = sortAlphabeticallyByName(getData(value));
    state = 'name-ascending';
    displayArrowUp(this);
    displayArrowDouble(arrowArea);
  }
  generateTr(sortedByName);
}

function sortOnArrowArea() {
  const arrowName = document.getElementById('arrow-name');
  const [{value}] = [...radioList].filter(radio => radio.checked);
  removeAllTd();
  let sortedByArea;
  if (state === 'area-ascending') {
    sortedByArea = sortByArea(getData(value)).reverse();
    state = 'area-descending';
    displayArrowDown(this);
    displayArrowDouble(arrowName);
  } else {
    sortedByArea = sortByArea(getData(value));
    state = 'area-ascending';
    displayArrowUp(this);
    displayArrowDouble(arrowName);
  }
  generateTr(sortedByArea);
}

appRoot.classList.add('main');
table.classList.add('table');
table.insertAdjacentHTML('afterbegin',
  `
  <tr>
    <th>Country name <button class="arrow" id="arrow-name">&#8593</button></th>
    <th>Capital</th>
    <th>World Region</th>
    <th>Languages</th>
    <th>Area <button class="arrow" id="arrow-area">&#8597</button></th>
    <th>Flag</th>
  </tr>
  `);

title.textContent = 'Countries Search';
title.classList.add('header-title');
insertElement(title);

form.innerHTML =
  `
    <div class="form-group">
      <p>Please choose type of search:</p>
        <div>
          <div>
            <input type="radio" id="radio-region"
                   name="search" value="region">
            <label for="radio-region">By region</label>
          </div>
          <div>
            <input type="radio" id="radio-language"
                    name="search" value="language">
            <label for="radio-language">By language</label>
          </div>
        </div>
    </div>
    <div>
      <label for="select-query">Please choose search query: </label>
      <select disabled id="select-query">
        <option selected value="default">Select value</option>
      </select>
    </div>
  `;
form.classList.add('header-form');
insertElement(form);

const select = document.getElementById('select-query');
const radioList = document.forms[0].search;

for (let radio of radioList) {
  radio.addEventListener('change', () => {
    generateOptions(radio.value);
  })
}

select.addEventListener('change', () => {
  const [{value}] = [...radioList].filter(radio => radio.checked);

  if (select.value !== 'default') {
    removeDefaultStr();
    const data = getData(value);
    generateTable(data);
  }
});
