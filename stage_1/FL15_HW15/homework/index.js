/* START TASK 1: Your code goes here */
const cells = document.querySelectorAll('.cell');
const table = document.querySelector('.table');

function toggleYellow(el) {
  el.classList.toggle('yellow');
  el.classList.remove('blue');
  el.classList.remove('green');
}

function toggleBlue(el) {
  el.classList.toggle('blue');
  el.classList.remove('yellow');
  el.classList.remove('green');
}

function toggleGreen(el) {
  el.classList.toggle('green');
  el.classList.remove('blue');
  el.classList.remove('yellow');
}

function toggleGreenHandler() {
  toggleGreen(table);
  for (let cell of cells) {
    if (!cell.classList.contains('yellow') && !cell.classList.contains('blue')) {
      toggleGreen(cell);
    }
  }
}

function toggleRow(el) {
  const rowToToggle = [];
  for (let cell of cells) {
    if (cell.dataset.row === el.dataset.row) {
      if (!cell.classList.contains('yellow')) {
        rowToToggle.push(cell);
      } else {
        rowToToggle.length = 0
        break;
      }
    }
  }
  rowToToggle.forEach(el => toggleBlue(el));
}

table.addEventListener('click', (e) => {
  if (e.target.id === 'special') {
    e.target.addEventListener('click', toggleGreenHandler);
    return;
  }

  if (e.target.dataset.column === '1') {
    toggleRow(e.target);
    return;
  }

  if(e.target.classList.contains('cell')) {
    toggleYellow(e.target);
  }
})
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const form = document.getElementById('form');
const input = document.getElementById('form-input');
const button = document.getElementById('submit-form-button');
const messageBox = document.getElementById('validation-massage')
const VALID_REG = /^\+380[0-9]{9}$/;

function validMessage() {
  messageBox.textContent = 'Data was sent successfully!';
  messageBox.classList.add('valid-massage-box');
  messageBox.classList.remove('invalid-massage-box');
}

function invalidMessage() {
  messageBox.textContent = 'The number does not follow the format +380*********';
  messageBox.classList.add('invalid-massage-box');
  messageBox.classList.remove('valid-massage-box');
}

function clearMessage() {
  messageBox.textContent = '';
  messageBox.classList.remove('valid-massage-box');
  messageBox.classList.remove('invalid-massage-box');
}

input.addEventListener('input', () => {
  if (VALID_REG.test(input.value)) {
    input.classList.remove('invalid');
    input.classList.add('valid');
    button.removeAttribute('disabled', true);
    clearMessage();
    return;
  }
  input.classList.add('invalid');
  input.classList.remove('valid');
  button.setAttribute('disabled', true);
  invalidMessage();
});

form.addEventListener('submit', (e) => {
  e.preventDefault();
  input.classList.remove('valid');
  validMessage();
});
/* END TASK 2 */

/* START TASK 3: Your code goes here */
const court = document.getElementById('court');
const ball = document.getElementById('ball');
const goalA = document.getElementById('goalA-zone');
const goalB = document.getElementById('goalB-zone');
const goalMessagebox = document.getElementById('goal-message-box');
const goalEvent = new CustomEvent('goal', {
  detail: {
    A: document.getElementById('A-score'),
    B: document.getElementById('B-score')
  }
});
let timerToDeleteMessage;

function moveBall(event) {
  if ([goalB, goalA].includes(event.target)) {
    const top = parseInt(getComputedStyle(event.target).top);
    const left = parseInt(getComputedStyle(event.target).left) + 10;
    ball.style.top = `${top}px`;
    ball.style.left = `${left}px`;
    goalEvent.targetTeam = event.target;
    court.dispatchEvent(goalEvent);
    return;
  }
  const x = event.pageX - event.target.x
  const y = event.pageY - (event.target.y + document.documentElement.scrollTop);
  ball.style.top = `${y}px`;
  ball.style.left = `${x}px`;
}

function deleteGoalMessage() {
  goalMessagebox.textContent = '';
}


court.addEventListener('click', (event) => {
  moveBall(event);
});

court.addEventListener('goal', (event) => {
  const team = event.targetTeam.dataset.team;
  const currentScore = parseInt(event.detail[`${team}`].textContent);

  clearInterval(timerToDeleteMessage);
  goalMessagebox.textContent = `Team ${team} scores!`;
  goalMessagebox.style.color = event.targetTeam.dataset.textColor;
  event.detail[`${team}`].textContent = currentScore + 1;
  timerToDeleteMessage = setTimeout(deleteGoalMessage, 3000);
});

for (let zone of [goalA, goalB]) {
  zone.addEventListener('click', (event) => {
    moveBall(event);
  });
}
/* END TASK 3 */