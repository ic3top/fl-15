const modifyItem = document.getElementById('modifyItem');
const tweetItems = document.getElementById('tweetItems');
const navigationButtons = document.getElementById('navigationButtons');
const alertElement = document.getElementById('alertMessage');
const list = document.getElementById('list');
const header = document.querySelector('h1');
// Buttons
const addTweetButton = document.querySelector('.addTweet');
const cancelModification = document.getElementById('cancelModification');
const saveModifiedItem = document.getElementById('saveModifiedItem');
const backButton = document.createElement('button');
backButton.textContent = 'Back';
// Text area
const modifyItemInput = document.getElementById('modifyItemInput');
const modifyItemHeader = document.getElementById('modifyItemHeader');

// Data storage
const tweets = JSON.parse(localStorage.getItem('tweets')) || {};

const newId = {
  current: JSON.parse(localStorage.getItem('ID')) || 0,
  get id() {
    this.current += 1;
    return this.current;
  }
};

// Timer for showAlert
let timer;

// render localStorage
Object.entries(tweets).forEach(([key, {
  message,
  liked
}]) => {
  addItemHTML(message, liked, key);
  toggleLikeBtn();
});

// Functions
function storageSave(data) {
  localStorage.setItem('tweets', JSON.stringify(data));
  localStorage.setItem('ID', JSON.stringify(newId.current));
}

function showAlert(msg, type = 'success') {
  if (timer) {
    clearTimeout(timer);
  }
  alertElement.classList.remove('hidden');
  type === 'error' ? alertElement.classList.add('error') : alertElement.classList.add('success');
  alertElement.textContent = msg;

  timer = setTimeout(() => {
    alertElement.className = '';
    alertElement.classList.add('hidden');
  }, 2000);
}

function showMainPage() {
  tweetItems.style.display = 'flex';
  modifyItem.classList.add('hidden');
  location.hash = '';
  header.textContent = 'Simple Twitter';
  navigationButtons.style.display = 'block';
  backButton.style.display = 'none';
  Object.entries(tweets).forEach(([key, { liked }]) => {
    if (!liked) {
      const liToHide = document.querySelector(`li[data-key="${key}"]`);
      liToHide.style.display = 'flex';
    }
  });
}

function getItemTemplate(msg, liked = false, key = null) {
  const li = document.createElement('li');
  const span = document.createElement('span');
  if (liked) {
    li.classList.add('itemLiked');
  }
  span.textContent = msg;
  li.classList.add('listItem');
  const btnGroup = document.createElement('div');

  const deleteButton = document.createElement('button');
  deleteButton.textContent = 'delete';
  deleteButton.classList.add('deleteBtn');

  const likeBtn = document.createElement('button');
  likeBtn.textContent = 'like';
  likeBtn.classList.add('likeBtn');

  if (key) {
    li.dataset.key = key;
    likeBtn.dataset.key = key;
    deleteButton.dataset.key = key;
  } else {
    li.dataset.key = newId.current;
    likeBtn.dataset.key = newId.current;
    deleteButton.dataset.key = newId.current;
  }

  btnGroup.append(deleteButton);
  btnGroup.append(likeBtn);
  li.append(span);
  li.append(btnGroup);
  return li;
}

function addItemHTML(msg, liked, key) {
  const li = getItemTemplate(msg, liked, key);
  list.insertAdjacentElement('beforeend', li);
}

function addItemHandler() {
  const tweet = modifyItemInput.value;
  if (Object.values(tweets).filter(({
      message
    }) => message === tweet).length > 0) {
    showAlert('Error! You can`t tweet about that again', 'error');
    return;
  }
  modifyItemInput.value = '';
  if (tweet && tweet.length < 140) {
    const uniqueId = newId.id;
    tweets[uniqueId] = {};
    tweets[uniqueId].message = tweet;
    addItemHTML(tweet);
    storageSave(tweets);
    showMainPage();
    return;
  }
  showAlert('Invalid input', 'error');
}

function deleteItem(key) {
  if (!confirm('Are you sure ?')) {
    return;
  }
  const toRemove = document.querySelector(`li[data-key="${key}"]`);
  delete tweets[key];
  storageSave(tweets);
  toggleLikeBtn();
  toRemove.remove();
}

function toggleLikeBtn() {
  const showLikedBtn = document.getElementById('likeBtn');
  if (Object.values(tweets).filter(el => el.liked).length !== 0) {
    if (!showLikedBtn) {
      const likeBtn = document.createElement('button');
      likeBtn.addEventListener('click', () => {
        location.hash = '/liked';
      });
      likeBtn.id = 'likeBtn';
      likeBtn.textContent = 'SHOW LIKED';
      navigationButtons.append(likeBtn);
    }
  } else if (showLikedBtn) {
    showLikedBtn.remove();
  }
}

function likeItem(key) {
  const toLike = document.querySelector(`li[data-key="${key}"]`);
  if (tweets[key].liked) {
    toLike.classList.remove('itemLiked');
    tweets[key].liked = false;
    showAlert(`Sorry you no longer like tweet with id ${key}`);
    storageSave(tweets);
    toggleLikeBtn();
    return;
  }
  toLike.classList.add('itemLiked');
  tweets[key].liked = true;
  showAlert(`Hooray! You liked tweet with id ${key}`);
  storageSave(tweets);
  toggleLikeBtn();
}

function showEditPage(key) {
  tweetItems.style.display = 'none';
  modifyItem.classList.remove('hidden');
  modifyItemHeader.textContent = 'Edit tweet';
  modifyItemInput.value = tweets[key].message;
  saveModifiedItem.onclick = () => {
    const tweet = modifyItemInput.value;
    if (Object.values(tweets).filter(({
        message
      }) => message === tweet).length > 0) {
      showAlert('Error: you can`t tweet about that again', 'error');
      return;
    }
    modifyItemInput.value = '';
    if (tweet && tweet.length < 140) {
      const span = document.querySelector(`li[data-key="${key}"]`).querySelector('span');
      span.textContent = tweet;
      tweets[key].message = tweet;
      showMainPage();
      return;
    }
    showAlert('Invalid input', 'error');
  };
}

function showLikedTweets() {
  header.textContent = 'Liked tweets';
  navigationButtons.style.display = 'none';
  backButton.style.display = 'block';
  tweetItems.insertBefore(backButton, list);
  Object.entries(tweets).forEach(([key, { liked }]) => {
    if (!liked) {
      const liToHide = document.querySelector(`li[data-key="${key}"]`);
      liToHide.style.display = 'none';
    }
  });
}

function router() {
  if (location.hash === '#/add') {
    tweetItems.style.display = 'none';
    modifyItem.classList.remove('hidden');
    modifyItemHeader.textContent = 'Add tweet';
    saveModifiedItem.onclick = addItemHandler;
    return;
  }
  if (!location.hash) {
    showMainPage();
    return;
  }
  if (/edit\//.test(location.hash)) {
    showEditPage(/\/:(.*)/.exec(location.hash)[1]);
    return;
  }
  if (location.hash === '#/liked') {
    showLikedTweets();
    return;
  }
}

// Listeners
window.onload = router;
window.addEventListener('hashchange', router);

addTweetButton.addEventListener('click', () => {
  location.hash = '/add';
});

cancelModification.addEventListener('click', () => {
  modifyItemInput.value = '';
  location.hash = '';
});

backButton.addEventListener('click', () => {
  location.hash = '';
});


list.addEventListener('click', function (event) {
  if (event.target.classList.contains('deleteBtn')) {
    deleteItem(event.target.dataset.key);
    return;
  }
  if (event.target.classList.contains('likeBtn')) {
    likeItem(event.target.dataset.key);
    return;
  }

  if (event.target.dataset.key) {
    location.hash = `edit/:${event.target.dataset.key}`;
  }
});