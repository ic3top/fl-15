(function () {
	let min = 1000;
	const koef = 100;
	const fixed = 2;

	const deposit = parseFloat(prompt('Input initial amount of money'));
	const years = Number(prompt('Input number of years'));
	const percentage = parseFloat(prompt('Input percentage of a year'));

	if(deposit < min || years < 1 || percentage > koef) {
		alert('Invalid input data');
		return;
	}

	const result = (deposit * Math.pow(1 + percentage / koef, years)).toFixed(fixed);
	const profit = (result - deposit).toFixed(fixed);

	alert(`Initial amount: ${deposit}
Numbers of years: ${years}
Percentage of year: ${percentage}

Total profit: ${profit}
Total amount: ${result}`);
}());
