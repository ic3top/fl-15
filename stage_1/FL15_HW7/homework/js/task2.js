/* eslint-disable no-magic-numbers */
let totalPrize = 0;
let pull = 100;
let max = 9;

function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}

function reset() {
	pull = 100;
	totalPrize = 0;
	max = 9;
	game();
}

function restart() {
	alert(`Thank you for your participation. Your prize is ${totalPrize}$.`);
	if (confirm('Do you want to play again?')) {
		reset();
	}
}

function game() {
	let prize = pull;
	const number = getRandomInt(max);

	for(let i = 1; i <= 3; i++) {
		const guess = Number(prompt(`Choose a roulette pocket number from 0 to ${max-1}
Attempts left: ${4-i}
Total prize: ${totalPrize}$
Possible prize on current attempt: ${prize}$`));

		if (number === guess) {
			totalPrize += prize;
			const confirmation = confirm(`Congratulation, you won! Your prize is: ${prize}$. \
Do you want to continue?`);

			if(confirmation) {
				max += 4;
				pull *= 2;
				game();

				return;
			} else {
				restart();

				return;
			}
		} 
		prize /= 2;
	}
	restart();
}

if (!confirm('Do you want to play a game?')) {
	alert('You did not become a billionaire, but can.');
} else {
	game();
}
