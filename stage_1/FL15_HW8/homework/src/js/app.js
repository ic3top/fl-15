/* eslint-disable no-magic-numbers */
document.addEventListener('DOMContentLoaded', () => {
  const eventName = prompt('Event name:', 'meeting');
  const nameInput = document.getElementById('name');
  const timeInput = document.getElementById('time');
  const placeInput = document.getElementById('place');
  const submitBtn = document.getElementById('submit');
  const convertBtn = document.getElementById('converter');
  const usd = 27.65;
  const eur = 32.92;


  function validate() {
    const name = nameInput.value;
    const time = timeInput.value;
    const place = placeInput.value;

    if (name && time && place) {
      if (/^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/.test(timeInput.value)) {
        console.log(`${name} has a ${eventName} today at ${time} ${place}`);
      } else {
        alert('Enter time in format hh:mm');

        return;
      }

      return;
    }
    alert('Input all date');
  }

  function convert() {
    const euros = parseFloat(prompt('Amount of euro:', 1));
    const dollars = parseFloat(prompt('Amount of dollars:', 1));

    if(euros > 0 && dollars > 0) {
      alert(`${euros} euros are equal ${(euros*eur).toFixed(2)}hrns, \
${dollars} dollars are equal ${(dollars*usd).toFixed(2)}hrns`);
      return;
    } 
    alert('Amount must be greater than 0');
  }

  submitBtn.addEventListener('click', (e) => {
    e.preventDefault();

    validate();
  });

  convertBtn.addEventListener('click', (e) => {
    e.preventDefault();

    convert();
  });
});