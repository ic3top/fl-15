function reverseNumber(num) {
  const numStr = String(num);
  let result = '';
  
  for (let i = numStr.length - 1; i >= 0; i--) {
    result += numStr[i];
  }

  result = parseFloat(result) * Math.sign(num);
  
  return result;
}

function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++) {
    func(arr[i]);
  }
}

function map(arr, func) {
	const copy = [];
	const newFunc = (el) => { 
  	copy.push(func(el));
  }
  forEach(arr, newFunc);
  
	return copy;
}

function filter(arr, func) {
  const copy = [];
  const newFunc = (el) => { 
    if(func(el)) {
      copy.push(el);
    }
  }
  forEach(arr, newFunc);

  return copy;
}

function getAdultAppleLovers(data) {
  const restriction = 18;
  const filteredArr = filter(data, ({ age,favoriteFruit}) => { 
    return age > restriction && favoriteFruit === 'apple';
  });

  return map(filteredArr, ({ name }) => {
    return name; 
  });
}

function getKeys(obj) {
  const result = [];
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      result.push(key);
    }
  }

  return result;
}

function getValues(obj) {
  const values = [];
  for (let key of getKeys(obj)) {
    values.push(obj[key]);
  }

  return values;
}

function showFormattedDate(dateObj) {
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const day = dateObj.getDate();
  const month = monthNames[dateObj.getMonth()];
  const year = dateObj.getFullYear();

  return `It is ${day} of ${month}, ${year}`;
}
